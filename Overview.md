### Project Overview

#### 1. Create EKS cluster in AWS using a Terraform script
#### 2. Configure Ansible to connect to EKS cluster
#### 3. Deploy Deployment and Service component a cluster namespace
